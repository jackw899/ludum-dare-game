package com.state;

import com.Main;
import org.lwjgl.input.Keyboard;

import static org.lwjgl.opengl.GL11.*;

public class StateMenuComplete implements State {
    @Override
    public void checkInput() {
        if (Keyboard.isKeyDown(Keyboard.KEY_ESCAPE))
            Main.stateManager.setState("stateMenuMain");
    }

    @Override
    public void update() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void render() {
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glTranslatef(0, 0, 0);
        Main.assetManager.fonts.get("Tahoma20BoldBlack").drawString(5, 5, "Completed Game");
        Main.assetManager.fonts.get("Tahoma16PlainBlack").drawString(5, 40, "Final score: " + Main.player.coins);
        Main.assetManager.fonts.get("Tahoma16PlainBlack").drawString(5, 60, "Thanks for playing my first completed game! - Jack");
        Main.assetManager.fonts.get("Tahoma16PlainBlack").drawString(5, 80, "Press \"escape\" to go back to main menu");
    }

    @Override
    public void onEnter() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void onExit() {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
