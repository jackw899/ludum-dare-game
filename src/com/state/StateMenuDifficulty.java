package com.state;

import com.Main;
import org.lwjgl.input.Keyboard;
import org.lwjgl.util.vector.Vector2f;

import static org.lwjgl.opengl.GL11.*;

public class StateMenuDifficulty implements State {
    @Override
    public void checkInput() {
        if (Keyboard.isKeyDown(Keyboard.KEY_1)) {
            Main.stateManager.setState("statePlaying");
            Main.assetManager.loadLevel("level");
            Main.player.livesRemaining = 3;
            Main.player.reset();
        } else if (Keyboard.isKeyDown(Keyboard.KEY_2)) {
            Main.stateManager.setState("statePlaying");
            Main.assetManager.loadLevel("level");
            Main.player.livesRemaining = 1;
            Main.player.reset();
        } else if (Keyboard.isKeyDown(Keyboard.KEY_3)) {
            Main.stateManager.setState("statePlaying");
            Main.assetManager.loadLevel("level");
            Main.player.livesRemaining = 0;
            Main.player.reset();
        } else if (Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) {
            Main.stateManager.setState("stateMenuMain");
        }
    }

    @Override
    public void update() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void render() {
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glTranslatef(0, 0, 0);
        Main.assetManager.fonts.get("Tahoma20BoldBlack").drawString(5, 5, "Select Difficulty");
        Main.assetManager.fonts.get("Tahoma16PlainBlack").drawString(5, 40, "Press \"1\" for Easy ( 3 extra lives )");
        Main.assetManager.fonts.get("Tahoma16PlainBlack").drawString(5, 60, "Press \"2\" for Regular ( 1 extra life )");
        Main.assetManager.fonts.get("Tahoma16PlainBlack").drawString(5, 80, "Press \"3\" for Hard ( 0 extra lives )");
        Main.assetManager.fonts.get("Tahoma16PlainBlack").drawString(5, 100, "Press \"escape\" to go back to main menu");
    }

    @Override
    public void onEnter() {
        Main.inMenu = true;
    }

    @Override
    public void onExit() {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
