package com.objects;

import com.Main;
import org.lwjgl.opengl.Display;
import org.newdawn.slick.Color;
import org.lwjgl.util.vector.Vector2f;

import static org.lwjgl.Sys.getTime;

public class Player {

    public int health = 10;
    public int livesRemaining = 3;
    public int currentCheckpoint = 0;

    public Vector2f position;
    public Vector2f tilePosition;
    public Vector2f moveToTile;

    public int facing = 2;

    private float speed = 200;

    public boolean moving = false;

    public int redKeys = 0;
    public int yellowKeys = 0;
    public int blueKeys = 0;
    public int greenKeys = 0;

    public int coins = 0;

    public long hurtSoundTime = 0;

    public boolean hadReset = false;

    public Player() {
        moveToTile = new Vector2f(1, 1);
        tilePosition = new Vector2f(1, 1);
        position = new Vector2f(moveToTile.x * 64 + 16, moveToTile.y * 64 - 32);
    }

    public void reset() {
        redKeys = 0;
        yellowKeys = 0;
        blueKeys = 0;
        greenKeys = 0;
        currentCheckpoint = 0;
        tilePosition = new Vector2f(Main.assetManager.checkpoints.get(currentCheckpoint).x, Main.assetManager.checkpoints.get(currentCheckpoint).y);
        moveToTile = tilePosition;
        position.x = moveToTile.x * 64 + 16;
        position.y = moveToTile.y * 64 - 32;
        facing = (int) Main.assetManager.checkpoints.get(currentCheckpoint).z;
        health = 10;
        hurtSoundTime = 0;
        hadReset = true;
        coins = 0;
    }

    public void update() {

        if (health <= 0) {
            livesRemaining--;
            if (livesRemaining < 0) {
                currentCheckpoint = 0;
                livesRemaining = 3;
            }
            tilePosition = new Vector2f(Main.assetManager.checkpoints.get(currentCheckpoint).x, Main.assetManager.checkpoints.get(currentCheckpoint).y);
            moveToTile = tilePosition;
            position.x = moveToTile.x * 64 + 16;
            position.y = moveToTile.y * 64 - 32;
            facing = (int) Main.assetManager.checkpoints.get(currentCheckpoint).z;
            health = 10;
        } else if (health > 10) health = 10;

        if (Main.assetManager.tiles.containsKey((int) moveToTile.x + "x" + (int) moveToTile.y))
            if (Main.assetManager.tiles.get((int) moveToTile.x + "x" + (int) moveToTile.y).collidable) {
                moveToTile = tilePosition;
            }


        if (position.x != moveToTile.x * 64 + 16) {
            if (position.x > moveToTile.x * 64 + 16) {
                float newPos = position.x - (speed * Main.delta);
                if (newPos >= moveToTile.x * 64 + 16) position.x = newPos;
                else position.x = moveToTile.x * 64 + 16;
            } else {
                float newPos = position.x + (speed * Main.delta);
                if (newPos <= moveToTile.x * 64 + 16) position.x = newPos;
                else position.x = moveToTile.x * 64 + 16;
            }
        } else if (position.y != moveToTile.y * 64 - 32) {
            if (position.y > moveToTile.y * 64 - 32) {
                float newPos = position.y - (speed * Main.delta);
                if (newPos >= moveToTile.y * 64 - 32) position.y = newPos;
                else position.y = moveToTile.y * 64 - 32;
            } else {
                float newPos = position.y + (speed * Main.delta);
                if (newPos <= moveToTile.y * 64 - 32) position.y = newPos;
                else position.y = moveToTile.y * 64 - 32;
            }
        }

        if ((position.x - 16) % 64 + (position.y + 32) % 64 == 0) {
            moving = false;
            tilePosition = new Vector2f((int) (position.x - 16) / 64, (int) (position.y + 32) / 64);
        } else moving = true;

        if (!moving && Main.assetManager.items.containsKey((int) tilePosition.x + "x" + (int) tilePosition.y)) {
            if (Main.assetManager.items.get((int) tilePosition.x + "x" + (int) tilePosition.y).active) {
                String textureName = Main.assetManager.items.get((int) tilePosition.x + "x" + (int) tilePosition.y).textureName;
                if (textureName.equals("melon")) {
                    Main.assetManager.items.remove((int) tilePosition.x + "x" + (int) tilePosition.y);
                    health += 2;
                    Main.assetManager.sounds.get("healspell2").playAsSoundEffect(1, 1, false);
                } else if (textureName.equals("steak")) {
                    Main.assetManager.items.get((int) tilePosition.x + "x" + (int) tilePosition.y).active = false;
                    health += 5;
                    Main.assetManager.sounds.get("healspell3").playAsSoundEffect(1, 1, false);
                } else if (textureName.equals("strawberry")) {
                    Main.assetManager.items.get((int) tilePosition.x + "x" + (int) tilePosition.y).active = false;
                    health += 1;
                    Main.assetManager.sounds.get("healspell1").playAsSoundEffect(1, 1, false);
                } else if (textureName.equals("keyred")) {
                    Main.assetManager.items.get((int) tilePosition.x + "x" + (int) tilePosition.y).active = false;
                    redKeys++;
                    Main.assetManager.sounds.get("taskcomplete").playAsSoundEffect(1, 1, false);
                } else if (textureName.equals("keyyellow")) {
                    Main.assetManager.items.get((int) tilePosition.x + "x" + (int) tilePosition.y).active = false;
                    yellowKeys++;
                    Main.assetManager.sounds.get("taskcomplete").playAsSoundEffect(1, 1, false);
                } else if (textureName.equals("keyblue")) {
                    Main.assetManager.items.get((int) tilePosition.x + "x" + (int) tilePosition.y).active = false;
                    blueKeys++;
                    Main.assetManager.sounds.get("taskcomplete").playAsSoundEffect(1, 1, false);
                } else if (textureName.equals("keygreen")) {
                    Main.assetManager.items.get((int) tilePosition.x + "x" + (int) tilePosition.y).active = false;
                    greenKeys++;
                    Main.assetManager.sounds.get("taskcomplete").playAsSoundEffect(1, 1, false);
                } else if (textureName.equals("coinbronze")) {
                    Main.assetManager.items.get((int) tilePosition.x + "x" + (int) tilePosition.y).active = false;
                    coins++;
                    Main.assetManager.sounds.get("coinpickup").playAsSoundEffect(1, 1, false);
                } else if (textureName.equals("coinsilver")) {
                    Main.assetManager.items.get((int) tilePosition.x + "x" + (int) tilePosition.y).active = false;
                    coins += 5;
                    Main.assetManager.sounds.get("coinpickup").playAsSoundEffect(1, 1, false);
                } else if (textureName.equals("coingold")) {
                    Main.assetManager.items.get((int) tilePosition.x + "x" + (int) tilePosition.y).active = false;
                    coins += 10;
                    Main.assetManager.sounds.get("coinpickup").playAsSoundEffect(1, 1, false);
                } else if (textureName.equals("chest")) {
                    Main.assetManager.sounds.get("complete").playAsSoundEffect(1, 1, false);
                    Main.stateManager.setState("stateMenuComplete");
                }
            }
        }

        if (getTime() >= hurtSoundTime && hurtSoundTime != 0) {
            Main.assetManager.sounds.get("pain" + (Main.rand.nextInt(6) + 1)).playAsSoundEffect(1, 1, false);
            hurtSoundTime = 0;
        }
    }

    public void render() {
        Main.assetManager.spritesheets.get("character").getSprite(0, facing).draw(position.x, position.y);
    }
}
