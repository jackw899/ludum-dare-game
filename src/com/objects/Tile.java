package com.objects;

import com.Main;
import com.state.*;
import org.newdawn.slick.Color;
import org.lwjgl.util.vector.Vector2f;

public class Tile {
    public Vector2f position;
    public String textureName;
    public boolean collidable;
    public int rotation;

    public Tile(Vector2f position, String textureName, boolean collidable, int rotation) {
        this.position = position;
        this.textureName = textureName;
        this.collidable = collidable;
        this.rotation = rotation;
    }

    public void render() {
        if (Main.stateManager.currentState.getClass().isInstance(new StatePlaying()) && collidable) {
            StatePlaying sp = (StatePlaying) Main.stateManager.currentState;
            if (sp.levelEditorMode)
                Main.assetManager.sprites.get(textureName).drawSprite(new Vector2f(position.x * 64, position.y * 64), Color.red, rotation);
            else
                Main.assetManager.sprites.get(textureName).drawSprite(new Vector2f(position.x * 64, position.y * 64), Color.white, rotation);
        } else
            Main.assetManager.sprites.get(textureName).drawSprite(new Vector2f(position.x * 64, position.y * 64), Color.white, rotation);
    }
}
