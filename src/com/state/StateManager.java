package com.state;

import java.util.HashMap;
import java.util.Map;

import static org.lwjgl.opengl.GL11.*;

public class StateManager {

    public Map<String, State> states = new HashMap<String, State>();
    public State currentState = new StateEmpty();
    public String currentStateName = "StateEmpty";

    public void checkInput() {
        currentState.checkInput();
    }

    public void update() {
        currentState.update();
    }

    public void render() {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        currentState.render();
    }

    public void setState(String stateName) {
        currentState.onExit();
        currentState = states.get(stateName);
        currentStateName = stateName;
        currentState.onEnter();
    }

    public void addState(String stateName, State state) {
        states.put(stateName, state);
    }

}
