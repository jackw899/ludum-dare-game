package com.objects;

import com.Main;
import org.newdawn.slick.Color;
import org.lwjgl.util.vector.Vector2f;

import static org.lwjgl.Sys.getTime;

public class Item {
    public Vector2f position;
    public Vector2f offset;
    public String textureName;
    public int rotation;
    public long created;
    public boolean active = true;

    public int checkpoint = 0;
    public long removeAt = 0;

    public long damageTime = 0;

    public Item(Vector2f position, Vector2f offset, String textureName, int rotation, int checkpoint, long removeAt) {
        this.position = position;
        this.offset = offset;
        this.textureName = textureName;
        this.rotation = rotation;
        this.checkpoint = checkpoint;
        this.created = getTime();
        this.removeAt = removeAt;
    }

    public Item(Vector2f position, Vector2f offset, String textureName, int rotation, long removeAt) {
        this.position = position;
        this.offset = offset;
        this.textureName = textureName;
        this.rotation = rotation;
        this.checkpoint = 0;
        this.created = getTime();
        this.removeAt = removeAt;
    }

    public Item(Vector2f position, Vector2f offset, String textureName, int rotation, int checkpoint) {
        this.position = position;
        this.offset = offset;
        this.textureName = textureName;
        this.rotation = rotation;
        this.checkpoint = checkpoint;
        this.created = getTime();
    }

    public Item(Vector2f position, Vector2f offset, String textureName, int rotation) {
        this.position = position;
        this.offset = offset;
        this.textureName = textureName;
        this.rotation = rotation;
        this.checkpoint = 0;
        this.created = getTime();
    }


    public void update() {
        if (active) {
            if (textureName.equals("bomb")) {
                if (getTime() >= created + 500) {
                    int damage = 4 - (Math.abs((int) position.x - (int) Main.player.tilePosition.x) + Math.abs((int) position.y - (int) Main.player.tilePosition.y));
                    if (damage < 0) damage = 0;
                    Main.player.health -= damage;
                    Main.assetManager.sounds.get("explode").playAsSoundEffect(0.75f, 0.75f, false);
                    Main.player.hurtSoundTime = getTime() + 250;
                    active = false;
                }
            }
            if (textureName.equals("spikes") && damageTime + 500 < getTime() && Main.player.tilePosition.x == position.x && Main.player.tilePosition.y == position.y) {
                damageTime = getTime();
                Main.player.health--;
                Main.player.hurtSoundTime = getTime();
            }
            if (getTime() >= removeAt && removeAt != 0) active = false;
        }
    }

    public void render() {
        if (active) {
            Main.assetManager.sprites.get(textureName).drawSprite(new Vector2f(position.x * 64 + offset.x, position.y * 64 + offset.y), Color.white, rotation);
            if (textureName.startsWith("door")) {
                //Main.assetManager.fonts.get("Tahoma10PlainGreen").drawString((position.x * 64) + 29, (position.y * 64) + 35, "" + checkpoint);
            }
        }
    }
}
