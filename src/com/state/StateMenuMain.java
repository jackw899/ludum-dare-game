package com.state;

import com.Main;
import org.lwjgl.input.Keyboard;

import static org.lwjgl.opengl.GL11.*;

public class StateMenuMain implements State {
    @Override
    public void checkInput() {
        if (Keyboard.isKeyDown(Keyboard.KEY_P)) {
            Main.stateManager.setState("stateMenuDifficulty");
        } else if (Keyboard.isKeyDown(Keyboard.KEY_ESCAPE)) {
            System.exit(0);
        }
    }

    @Override
    public void update() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void render() {
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glTranslatef(0, 0, 0);
        Main.assetManager.fonts.get("Tahoma20BoldBlack").drawString(5, 5, "10 Second Mayhem");
        Main.assetManager.fonts.get("Tahoma16PlainBlack").drawString(5, 40, "Press \"p\" to play");
        Main.assetManager.fonts.get("Tahoma16PlainBlack").drawString(5, 60, "Press \"escape\" to quit");
        Main.assetManager.fonts.get("Tahoma16PlainBlack").drawString(5, 100, "Controls: WASD to move, F to use keys, escape to enter/exit pause menu");


        Main.assetManager.fonts.get("Tahoma16PlainBlack").drawString(5, 140, "About game: Collect keys and unlock doors to get to the next room, you need to open the doors in the correct");
        Main.assetManager.fonts.get("Tahoma16PlainBlack").drawString(5, 160, "order or you will be stuck. Going through a yellow door unlocks the checkpoint for that room. Strawberry's heal");
        Main.assetManager.fonts.get("Tahoma16PlainBlack").drawString(5, 180, "1 heart, Melon's heal 2 hearts and Steak heals 5 hearts. The final room is an invisible maze so you have to work");
        Main.assetManager.fonts.get("Tahoma16PlainBlack").drawString(5, 200, "out how to get through it! Collect coins to get more points once you complete the game.");
        Main.assetManager.fonts.get("Tahoma16PlainBlack").drawString(5, 220, "Good luck and have fun!");


    }

    @Override
    public void onEnter() {
        Main.inMenu = true;
    }

    @Override
    public void onExit() {
    }
}
