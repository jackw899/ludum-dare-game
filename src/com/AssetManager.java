package com;


import com.objects.Item;
import com.objects.Sprite;
import com.objects.Tile;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.SpriteSheet;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;
import org.newdawn.slick.openal.Audio;
import org.newdawn.slick.openal.AudioLoader;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

import java.awt.*;
import java.io.*;
import java.util.*;
import java.util.List;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL11.GL_LINEAR;

public class AssetManager {

    public Map<String, Texture> textures = new LinkedHashMap<String, Texture>();
    public Map<String, Sprite> sprites = new HashMap<String, Sprite>();
    public Map<String, SpriteSheet> spritesheets = new HashMap<String, SpriteSheet>();
    public Map<String, UnicodeFont> fonts = new HashMap<String, UnicodeFont>();
    public Map<String, Audio> sounds = new HashMap<String, Audio>();
    public Map<String, Tile> tiles = new HashMap<String, Tile>();
    public Map<String, Item> items = new HashMap<String, Item>();

    public String[] tileTextures = new String[]{"sandtile", "sandtileedge", "sandtilecorner", "sandtiletriple", "stone"};
    public String[] itemTextures = new String[]{"wall", "wallcorner", "walltriple", "wallquad", "melon", "steak", "strawberry", "bomb", "doorred", "dooryellow", "doorblue", "doorgreen", "keyred", "keyyellow", "keyblue", "keygreen", "spikes", "heart", "coinbronze", "coinsilver", "coingold", "chest"};

    public List<Vector3f> checkpoints = new ArrayList<Vector3f>();

    public AssetManager() {
        loadTexture("PNG", "sandtile", "sandtile");
        loadTexture("PNG", "sandtileedge", "sandtileedge");
        loadTexture("PNG", "sandtilecorner", "sandtilecorner");
        loadTexture("PNG", "sandtiletriple", "sandtiletriple");
        loadTexture("PNG", "stone", "stone");
        loadTexture("PNG", "spikes", "spikes");


        loadTexture("PNG", "wall", "wall");
        loadTexture("PNG", "wallcorner", "wallcorner");
        loadTexture("PNG", "walltriple", "walltriple");
        loadTexture("PNG", "wallquad", "wallquad");
        loadTexture("PNG", "melon", "melon");
        loadTexture("PNG", "steak", "steak");
        loadTexture("PNG", "strawberry", "strawberry");
        loadTexture("PNG", "bomb", "bomb");
        loadTexture("PNG", "doorred", "doorred");
        loadTexture("PNG", "dooryellow", "dooryellow");
        loadTexture("PNG", "doorblue", "doorblue");
        loadTexture("PNG", "doorgreen", "doorgreen");
        loadTexture("PNG", "keyred", "keyred");
        loadTexture("PNG", "keyyellow", "keyyellow");
        loadTexture("PNG", "keyblue", "keyblue");
        loadTexture("PNG", "keygreen", "keygreen");

        loadTexture("PNG", "heartempty", "heartempty");
        loadTexture("PNG", "heart", "heart");
        loadTexture("PNG", "coinbronze", "coinbronze");
        loadTexture("PNG", "coinsilver", "coinsilver");
        loadTexture("PNG", "coingold", "coingold");
        loadTexture("PNG", "chest", "chest");
        loadTexture("PNG", "checkpoint", "checkpoint");


        loadSpritesheet("PNG", "characterspritesheet", new Vector2f(32, 64), "character");
        loadFont("Tahoma10PlainWhite", "Tahoma", 10, Font.PLAIN, Color.black);
        loadFont("Tahoma10PlainGreen", "Tahoma", 10, Font.PLAIN, Color.green);
        loadFont("Tahoma14PlainWhite", "Tahoma", 14, Font.PLAIN, Color.white);
        loadFont("Tahoma14PlainGreen", "Tahoma", 14, Font.PLAIN, Color.green);
        loadFont("Tahoma16PlainBlack", "Tahoma", 16, Font.PLAIN, Color.black);
        loadFont("Tahoma20BoldBlack", "Tahoma", 20, Font.BOLD, Color.black);

        loadSound("WAV", "explode", "explode");
        loadSound("WAV", "healspell1", "healspell1");
        loadSound("WAV", "healspell2", "healspell2");
        loadSound("WAV", "healspell3", "healspell3");
        loadSound("WAV", "taskcomplete", "taskcomplete");
        loadSound("WAV", "pain1", "pain1");
        loadSound("WAV", "pain2", "pain2");
        loadSound("WAV", "pain3", "pain3");
        loadSound("WAV", "pain4", "pain4");
        loadSound("WAV", "pain5", "pain5");
        loadSound("WAV", "pain6", "pain6");
        loadSound("WAV", "Menu_loop", "menu");
        loadSound("WAV", "dooropen", "dooropen");
        loadSound("WAV", "coinpickup", "coinpickup");
        loadSound("WAV", "complete", "complete");


        loadLevel("level");
    }

    public void loadTexture(String texExt, String texName, String shortName) {
        try {
            textures.put(shortName, TextureLoader.getTexture(texExt.toUpperCase(), ResourceLoader.getResourceAsStream("assets/" + texName + "." + texExt.toLowerCase())));
        } catch (IOException e) {
            e.printStackTrace();
        }

        textures.get(shortName).bind();
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

        sprites.put(shortName, new Sprite(textures.get(shortName)));
    }

    public void loadTexture(String texExt, String texName, String shortName, boolean flip) {
        try {
            textures.put(shortName, TextureLoader.getTexture(texExt.toUpperCase(), ResourceLoader.getResourceAsStream("assets/" + texName + "." + texExt.toLowerCase()), flip));
        } catch (IOException e) {
            e.printStackTrace();
        }

        textures.get(shortName).bind();
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        sprites.put(shortName, new Sprite(textures.get(shortName)));
    }

    public void loadSpritesheet(String texExt, String texName, Vector2f size, String shortName) {
        try {
            spritesheets.put(shortName, new SpriteSheet("assets/" + texName + "." + texExt.toLowerCase(), (int) size.x, (int) size.y));
        } catch (SlickException e) {
            e.printStackTrace();
        }

        spritesheets.get(shortName).bind();
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    }

    public void loadFont(String shortName, String fontName, int fontSize, int fontType, Color color) {
        Font awtFont = new Font(fontName, fontType, fontSize);
        UnicodeFont uFont = new UnicodeFont(awtFont);
        uFont.addAsciiGlyphs();
        uFont.getEffects().add(new ColorEffect(color));
        try {
            uFont.loadGlyphs();
        } catch (Exception e) {
            e.printStackTrace();
        }
        fonts.put(shortName, uFont);
    }

    public void loadSound(String soundExt, String soundFile, String shortName) {
        try {
            sounds.put(shortName, AudioLoader.getAudio(soundExt.toUpperCase(), ResourceLoader.getResourceAsStream("assets/" + soundFile + "." + soundExt.toLowerCase())));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void loadLevel(String levelName) {
        tiles.clear();
        items.clear();
        File levelFile = new File("assets/" + levelName + ".ld");
        if (!levelFile.exists()) {
            System.out.println("No level file found");
            System.exit(0);
        } else {
            try {
                BufferedReader br = new BufferedReader(new FileReader(levelFile.getAbsoluteFile()));

                String line = br.readLine();
                while (line != null) {
                    if (line.startsWith("tile")) {
                        String[] attributes = line.substring(("tile").length() + 1).split(" ");
                        boolean collidable = (attributes[3].equals("true")) ? true : false;
                        tiles.put(attributes[0] + "x" + attributes[1], new Tile(new Vector2f(Float.parseFloat(attributes[0]), Float.parseFloat(attributes[1])), attributes[2], collidable, Integer.parseInt(attributes[4])));
                    } else if (line.startsWith("item")) {
                        String[] attributes = line.substring(("item").length() + 1).split(" ");

                        Vector2f padding = new Vector2f();
                        if (attributes[2].equals("strawberry") || attributes[2].equals("melon") || attributes[2].equals("steak")
                                || attributes[2].equals("bomb") || attributes[2].startsWith("key") || attributes[2].startsWith("coin") || attributes[2].equals("chest"))
                            padding = new Vector2f(16, 16);


                        items.put(attributes[0] + "x" + attributes[1], new Item(new Vector2f(Float.parseFloat(attributes[0]), Float.parseFloat(attributes[1])), padding, attributes[2], Integer.parseInt(attributes[3]), Integer.parseInt(attributes[4])));
                    } else if (line.startsWith("checkpoint")) {
                        String[] attributes = line.substring(("checkpoint").length() + 1).split(" ");
                        checkpoints.add(new Vector3f(Float.parseFloat(attributes[0]), Float.parseFloat(attributes[1]), Float.parseFloat(attributes[2])));
                    }
                    line = br.readLine();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

    public void saveLevel() {
        String saveFileText = "// LEVEL FILE GENERATED BY JACK!";

        for (Map.Entry<String, Tile> entry : tiles.entrySet()) {
            Tile tile = entry.getValue();
            saveFileText += "\ntile " + (int) tile.position.x + " " + (int) tile.position.y + " " + tile.textureName + " " + tile.collidable + " " + tile.rotation;
        }

        for (Map.Entry<String, Item> entry : items.entrySet()) {
            Item item = entry.getValue();
            if (!item.textureName.equals("bomb") && !(item.textureName.equals("spikes") && !item.active))
                saveFileText += "\nitem " + (int) item.position.x + " " + (int) item.position.y + " " + item.textureName + " " + item.rotation + " " + item.checkpoint;
        }

        for (Vector3f checkpoint : checkpoints) {
            saveFileText += "\ncheckpoint " + (int) checkpoint.x + " " + (int) checkpoint.y + " " + (int) checkpoint.z;
        }

        try {
            File levelFile = new File("level.ld");
            if (!levelFile.exists()) levelFile.createNewFile();

            FileWriter fw = new FileWriter(levelFile.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(saveFileText);
            bw.close();

            System.out.println("Level Saved");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
