package com.state;

import com.Main;
import com.objects.Item;
import com.objects.Tile;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Vector3f;
import org.newdawn.slick.Color;
import org.lwjgl.util.vector.Vector2f;

import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.Set;

import static org.lwjgl.Sys.getTime;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL11.glLoadIdentity;
import static org.lwjgl.opengl.GL11.glTranslatef;

public class StatePlaying implements State {

    private static float movementSpeed = 100;
    private static long last10SecondEvent = 0;
    private static long last10SecondEventDif = 0;

    //LEVEL MAKER STUFF
    public int selectedID = 0;
    public String selectedTex = "sandtile";
    public boolean levelEditorMode = false;

    @Override
    public void checkInput() {

        Vector2f mousePos = new Vector2f(Mouse.getX(), Mouse.getY());
        mousePos.y = Display.getHeight() - Mouse.getY();
        Vector2f gridMousePos = new Vector2f(mousePos.x - Main.cameraPos.x, mousePos.y - Main.cameraPos.y);
        /*gridMousePos.x = (int) gridMousePos.x / 64;
        gridMousePos.y = (int) gridMousePos.y / 64;*/
        gridMousePos.x = (int) Math.floor(gridMousePos.x / 64);
        gridMousePos.y = (int) Math.floor(gridMousePos.y / 64);


        if (Keyboard.isKeyDown(Keyboard.KEY_W) && !Main.player.moving) {
            String key = (int) Main.player.moveToTile.x + "x" + (int) (Main.player.moveToTile.y - 1);
            if (Main.assetManager.tiles.containsKey(key))
                if (!Main.player.moving && !Main.assetManager.tiles.get(key).collidable)
                    if (Main.assetManager.items.containsKey(key)) {
                        if (!Main.assetManager.items.get(key).textureName.startsWith("door") || !Main.assetManager.items.get(key).active || levelEditorMode) {
                            Main.player.moveToTile.y -= 1;
                            Main.player.moving = true;
                        }
                    } else {
                        Main.player.moveToTile.y -= 1;
                        Main.player.moving = true;
                    }
            Main.player.facing = 0;
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_S) && !Main.player.moving) {
            String key = (int) Main.player.moveToTile.x + "x" + (int) (Main.player.moveToTile.y + 1);
            if (Main.assetManager.tiles.containsKey(key))
                if (!Main.player.moving && !Main.assetManager.tiles.get(key).collidable)
                    if (Main.assetManager.items.containsKey(key)) {
                        if (!Main.assetManager.items.get(key).textureName.startsWith("door") || !Main.assetManager.items.get(key).active || levelEditorMode) {
                            Main.player.moveToTile.y += 1;
                            Main.player.moving = true;
                        }
                    } else {
                        Main.player.moveToTile.y += 1;
                        Main.player.moving = true;
                    }
            Main.player.facing = 2;
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_A) && !Main.player.moving) {
            String key = (int) (Main.player.moveToTile.x - 1) + "x" + (int) Main.player.moveToTile.y;
            if (Main.assetManager.tiles.containsKey(key))
                if (!Main.player.moving && !Main.assetManager.tiles.get(key).collidable)
                    if (Main.assetManager.items.containsKey(key)) {
                        if (!Main.assetManager.items.get(key).textureName.startsWith("door") || !Main.assetManager.items.get(key).active || levelEditorMode) {
                            Main.player.moveToTile.x -= 1;
                            Main.player.moving = true;
                        }
                    } else {
                        Main.player.moveToTile.x -= 1;
                        Main.player.moving = true;
                    }
            Main.player.facing = 3;
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_D) && !Main.player.moving) {
            String key = (int) (Main.player.moveToTile.x + 1) + "x" + (int) Main.player.moveToTile.y;
            if (Main.assetManager.tiles.containsKey(key))
                if (!Main.player.moving && !Main.assetManager.tiles.get(key).collidable)
                    if (Main.assetManager.items.containsKey(key)) {
                        if (!Main.assetManager.items.get(key).textureName.startsWith("door") || !Main.assetManager.items.get(key).active || levelEditorMode) {
                            Main.player.moveToTile.x += 1;
                            Main.player.moving = true;
                        }
                    } else {
                        Main.player.moveToTile.x += 1;
                        Main.player.moving = true;
                    }
            Main.player.facing = 1;
        }
        if (Keyboard.isKeyDown(Keyboard.KEY_F)) {
            //USE KEY TO UNLOCK DOORS
            String key = "";
            if (Main.player.facing == 0)
                key = (int) Main.player.tilePosition.x + "x" + (int) (Main.player.tilePosition.y - 1);
            else if (Main.player.facing == 1)
                key = (int) (Main.player.tilePosition.x + 1) + "x" + (int) Main.player.tilePosition.y;
            else if (Main.player.facing == 2)
                key = (int) Main.player.tilePosition.x + "x" + (int) (Main.player.tilePosition.y + 1);
            else if (Main.player.facing == 3)
                key = (int) (Main.player.tilePosition.x - 1) + "x" + (int) Main.player.tilePosition.y;


            if (Main.assetManager.items.containsKey(key))
                if (Main.assetManager.items.get(key).textureName.startsWith("doorred") && Main.assetManager.items.get(key).active) {
                    if (Main.player.redKeys > 0) {
                        Main.player.redKeys--;
                        Main.assetManager.items.get(key).active = false;
                        Main.assetManager.sounds.get("dooropen").playAsSoundEffect(1, 1, false);
                    }
                } else if (Main.assetManager.items.get(key).textureName.startsWith("doorblue") && Main.assetManager.items.get(key).active) {
                    if (Main.player.blueKeys > 0) {
                        Main.player.blueKeys--;
                        Main.assetManager.items.get(key).active = false;
                        Main.assetManager.sounds.get("dooropen").playAsSoundEffect(1, 1, false);
                    }
                } else if (Main.assetManager.items.get(key).textureName.startsWith("doorgreen") && Main.assetManager.items.get(key).active) {
                    if (Main.player.greenKeys > 0) {
                        Main.player.greenKeys--;
                        Main.assetManager.items.get(key).active = false;
                        Main.assetManager.sounds.get("dooropen").playAsSoundEffect(1, 1, false);
                    }
                } else if (Main.assetManager.items.get(key).textureName.startsWith("dooryellow") && Main.assetManager.items.get(key).active) {
                    if (Main.player.yellowKeys > 0) {
                        if (Main.assetManager.items.get(key).checkpoint != 0)
                            Main.player.currentCheckpoint = Main.assetManager.items.get(key).checkpoint;
                        Main.player.yellowKeys--;
                        Main.assetManager.items.get(key).active = false;
                        Main.assetManager.sounds.get("dooropen").playAsSoundEffect(1, 1, false);
                    }
                }
        }

        while (Keyboard.next()) {
            if (Keyboard.getEventKeyState()) {
                if (Keyboard.getEventKey() == Keyboard.KEY_ESCAPE) Main.stateManager.setState("stateMenuPause");
                else if (Keyboard.getEventKey() == Keyboard.KEY_F9 && levelEditorMode)
                    Main.assetManager.saveLevel();
                else if (Keyboard.getEventKey() == Keyboard.KEY_F10)
                    levelEditorMode = false;
                    //levelEditorMode = !levelEditorMode;
                else if (Keyboard.getEventKey() == Keyboard.KEY_R && levelEditorMode) {
                    if (Arrays.asList(Main.assetManager.tileTextures).contains(selectedTex)) {
                        Main.assetManager.tiles.remove((int) gridMousePos.x + "x" + (int) gridMousePos.y);
                    } else if (Arrays.asList(Main.assetManager.itemTextures).contains(selectedTex)) {
                        Main.assetManager.items.remove((int) gridMousePos.x + "x" + (int) gridMousePos.y);
                    }
                } else if (Keyboard.getEventKey() == Keyboard.KEY_C && levelEditorMode) {
                    boolean exists = false;
                    for (Vector3f checkpoint : Main.assetManager.checkpoints) {
                        if ((int) checkpoint.x == (int) gridMousePos.x && (int) checkpoint.y == (int) gridMousePos.y) {
                            exists = true;
                            checkpoint.z++;
                            if (checkpoint.z == 4) checkpoint.z = 0;
                        }
                    }
                    if (!exists)
                        Main.assetManager.checkpoints.add(new Vector3f((int) gridMousePos.x, (int) gridMousePos.y, 0));
                } else if (Keyboard.getEventKey() == Keyboard.KEY_MINUS && levelEditorMode) {
                    int id = -1;
                    for (Vector3f checkpoint : Main.assetManager.checkpoints) {
                        if ((int) checkpoint.x == (int) gridMousePos.x && (int) checkpoint.y == (int) gridMousePos.y) {
                            id = Main.assetManager.checkpoints.indexOf(checkpoint);
                        }
                    }
                    if (id != -1) {
                        Vector3f checkpoint = Main.assetManager.checkpoints.get(id);
                        Main.assetManager.checkpoints.set(id, Main.assetManager.checkpoints.get(id - 1));
                        Main.assetManager.checkpoints.set(id - 1, checkpoint);
                    } else {
                        for (Map.Entry<String, Item> entry : Main.assetManager.items.entrySet()) {
                            Item item = entry.getValue();
                            if ((int) item.position.x == (int) gridMousePos.x && (int) item.position.y == (int) gridMousePos.y) {
                                item.checkpoint--;
                                if (item.checkpoint < 0) item.checkpoint = Main.assetManager.checkpoints.size() - 1;
                            }
                        }
                    }
                } else if (Keyboard.getEventKey() == Keyboard.KEY_EQUALS && levelEditorMode) {
                    int id = -1;
                    for (Vector3f checkpoint : Main.assetManager.checkpoints) {
                        if ((int) checkpoint.x == (int) gridMousePos.x && (int) checkpoint.y == (int) gridMousePos.y) {
                            id = Main.assetManager.checkpoints.indexOf(checkpoint);
                        }
                    }
                    if (id != -1) {
                        Vector3f checkpoint = Main.assetManager.checkpoints.get(id);
                        Main.assetManager.checkpoints.set(id, Main.assetManager.checkpoints.get(id + 1));
                        Main.assetManager.checkpoints.set(id + 1, checkpoint);
                    } else {
                        for (Map.Entry<String, Item> entry : Main.assetManager.items.entrySet()) {
                            Item item = entry.getValue();
                            if ((int) item.position.x == (int) gridMousePos.x && (int) item.position.y == (int) gridMousePos.y) {
                                item.checkpoint++;
                                if (item.checkpoint < -1) item.checkpoint = Main.assetManager.checkpoints.size() - 1;
                                if (item.checkpoint >= Main.assetManager.checkpoints.size()) item.checkpoint = -1;
                            }
                        }
                    }
                }
            }
        }

        while (Mouse.next()) {
            if (!Mouse.getEventButtonState()) {
                if (Mouse.getEventButton() == 0) {
                    if (levelEditorMode) {
                        if (Arrays.asList(Main.assetManager.tileTextures).contains(selectedTex)) {
                            if (!Main.assetManager.tiles.containsKey((int) gridMousePos.x + "x" + (int) gridMousePos.y)) {
                                Main.assetManager.tiles.put((int) gridMousePos.x + "x" + (int) gridMousePos.y, new Tile(new Vector2f(gridMousePos.x, gridMousePos.y), selectedTex, false, 0));
                            } else {
                                Main.assetManager.tiles.get((int) gridMousePos.x + "x" + (int) gridMousePos.y).textureName = selectedTex;
                            }
                        } else if (Arrays.asList(Main.assetManager.itemTextures).contains(selectedTex)) {
                            Vector2f padding = new Vector2f();
                            if (selectedTex.equals("strawberry") || selectedTex.equals("melon") || selectedTex.equals("steak")
                                    || selectedTex.equals("bomb") || selectedTex.startsWith("key") || selectedTex.startsWith("coin") || selectedTex.equals("chest"))
                                padding = new Vector2f(16, 16);
                            if (!Main.assetManager.items.containsKey((int) gridMousePos.x + "x" + (int) gridMousePos.y)) {
                                Main.assetManager.items.put((int) gridMousePos.x + "x" + (int) gridMousePos.y, new Item(new Vector2f(gridMousePos.x, gridMousePos.y), padding, selectedTex, 0));
                            } else {
                                Main.assetManager.items.get((int) gridMousePos.x + "x" + (int) gridMousePos.y).textureName = selectedTex;
                            }
                        }
                    }
                } else if (Mouse.getEventButton() == 1) {
                    if (levelEditorMode) {
                        if (Arrays.asList(Main.assetManager.tileTextures).contains(selectedTex)) {
                            if (Main.assetManager.tiles.containsKey((int) gridMousePos.x + "x" + (int) gridMousePos.y)) {
                                if (Main.assetManager.tiles.get((int) gridMousePos.x + "x" + (int) gridMousePos.y).rotation == 270)
                                    Main.assetManager.tiles.get((int) gridMousePos.x + "x" + (int) gridMousePos.y).rotation = 0;
                                else
                                    Main.assetManager.tiles.get((int) gridMousePos.x + "x" + (int) gridMousePos.y).rotation += 90;
                            }
                        } else if (Arrays.asList(Main.assetManager.itemTextures).contains(selectedTex)) {
                            if (Main.assetManager.items.containsKey((int) gridMousePos.x + "x" + (int) gridMousePos.y)) {
                                if (Main.assetManager.items.get((int) gridMousePos.x + "x" + (int) gridMousePos.y).rotation == 270)
                                    Main.assetManager.items.get((int) gridMousePos.x + "x" + (int) gridMousePos.y).rotation = 0;
                                else
                                    Main.assetManager.items.get((int) gridMousePos.x + "x" + (int) gridMousePos.y).rotation += 90;
                            }
                        }
                    }
                } else if (Mouse.getEventButton() == 2 && levelEditorMode) {
                    Main.assetManager.tiles.get((int) gridMousePos.x + "x" + (int) gridMousePos.y).collidable = !Main.assetManager.tiles.get((int) gridMousePos.x + "x" + (int) gridMousePos.y).collidable;
                }
            }
        }

        int dWheel = Mouse.getDWheel();
        if (dWheel < 0) {
            //DOWN
            Set<String> textureKeys = Main.assetManager.textures.keySet();
            selectedID++;
            if (selectedID >= textureKeys.size()) selectedID = 0;
            selectedTex = textureKeys.toArray(new String[0])[selectedID];
        } else if (dWheel > 0) {
            //UP
            Set<String> textureKeys = Main.assetManager.textures.keySet();
            selectedID--;
            if (selectedID < 0) selectedID = textureKeys.size() - 1;
            selectedTex = textureKeys.toArray(new String[0])[selectedID];
        }
    }

    @Override
    public void update() {
        if (getTime() >= last10SecondEvent + 10000 && !levelEditorMode) {
            int rand = Main.rand.nextInt(2);
            //System.out.println("10 second event...");
            last10SecondEvent = getTime();

            if (rand == 0) {
                Vector2f pos = new Vector2f();
                boolean posFound = false;
                while (!posFound) {
                    pos = new Vector2f(Main.player.tilePosition.x - 1 + Main.rand.nextInt(3), Main.player.tilePosition.y - 1 + Main.rand.nextInt(3));
                    if (!Main.assetManager.tiles.containsKey((int) pos.x + "x" + (int) pos.y))
                        posFound = true;
                    else if (!Main.assetManager.tiles.get((int) pos.x + "x" + (int) pos.y).collidable)
                        posFound = true;

                }
                Main.assetManager.items.put(pos.x + "x" + pos.y, new Item(pos, new Vector2f(16, 16), "bomb", 0));
            } else
                Main.assetManager.items.put(Main.player.tilePosition.x + "x" + Main.player.tilePosition.y, new Item(new Vector2f(Main.player.tilePosition.x, Main.player.tilePosition.y), new Vector2f(0, 0), "spikes", 0, getTime() + 1000));
        }

        Main.player.update();

        for (Map.Entry<String, Item> entry : Main.assetManager.items.entrySet()) {
            Item item = entry.getValue();
            item.update();
        }

        Main.cameraPos.x = ((Display.getWidth()) / 2) - 16 - Main.player.position.x;
        Main.cameraPos.y = ((Display.getHeight()) / 2) - 32 - Main.player.position.y;
    }

    @Override
    public void render() {
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glTranslatef(Main.cameraPos.x, Main.cameraPos.y, 0);
        glEnable(GL_TEXTURE_2D);

        for (Map.Entry<String, Tile> entry : Main.assetManager.tiles.entrySet()) {
            Tile tile = entry.getValue();
            tile.render();
        }

        for (Map.Entry<String, Item> entry : Main.assetManager.items.entrySet()) {
            Item item = entry.getValue();
            item.render();
        }

        /*for (Vector3f checkpoint : Main.assetManager.checkpoints) {
            Main.assetManager.sprites.get("checkpoint").drawSprite(new Vector2f((checkpoint.x * 64) + 16, (checkpoint.y * 64) + 16), Color.white, (int) checkpoint.z * 90);
            Main.assetManager.fonts.get("Tahoma10PlainWhite").drawString((checkpoint.x * 64) + 29, (checkpoint.y * 64) + 30, "" + Main.assetManager.checkpoints.indexOf(checkpoint));
        }*/

        Main.player.render();


        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glTranslatef(0, 0, 0);
        //Main.assetManager.fonts.get("Tahoma14PlainWhite").drawString(5, 5, "Health:" + Main.player.health);

        Main.assetManager.fonts.get("Tahoma16PlainBlack").drawString(5, 38, Main.player.livesRemaining + " lives remaining");
        if (levelEditorMode) {

            Main.assetManager.sprites.get(selectedTex).drawSprite(new Vector2f(5, 75), Color.white);
            Main.assetManager.fonts.get("Tahoma14PlainGreen").drawString(5, 55, selectedTex);
        }

        for (int i = 0; i < Main.player.redKeys; i++) {
            Main.assetManager.sprites.get("keyred").drawSprite(new Vector2f(5 + (i * 37), 526), Color.white, 0);
        }
        for (int i = 0; i < Main.player.yellowKeys; i++) {
            Main.assetManager.sprites.get("keyyellow").drawSprite(new Vector2f(763 - (i * 37), 526), Color.white, 0);
        }
        for (int i = 0; i < Main.player.blueKeys; i++) {
            Main.assetManager.sprites.get("keyblue").drawSprite(new Vector2f(5 + (i * 37), 563), Color.white, 0);
        }
        for (int i = 0; i < Main.player.greenKeys; i++) {
            Main.assetManager.sprites.get("keygreen").drawSprite(new Vector2f(763 - (i * 37), 563), Color.white, 0);
        }

        for (int i = 0; i < 10; i++) {
            if (i < Main.player.health)
                Main.assetManager.sprites.get("heart").drawSprite(new Vector2f(5 + (i * 34), 5), Color.white, 0);
            else
                Main.assetManager.sprites.get("heartempty").drawSprite(new Vector2f(5 + (i * 34), 5), Color.white, 0);
        }
    }

    @Override
    public void onEnter() {
        if (Main.player.hadReset) {
            Main.player.hadReset = false;
            last10SecondEvent = getTime();
        } else {
            last10SecondEvent = getTime() - last10SecondEventDif;
        }
        Main.inMenu = false;
    }

    @Override
    public void onExit() {
        last10SecondEventDif = getTime() - last10SecondEvent;
    }
}
