package com.objects;

import org.newdawn.slick.Color;
import org.lwjgl.util.vector.Vector2f;
import org.newdawn.slick.opengl.Texture;

import static org.lwjgl.opengl.GL11.*;

public class Sprite {

    Texture texture;
    Vector2f size;

    public Sprite(Texture tex) {
        texture = tex;
        size = new Vector2f(tex.getImageWidth(), tex.getImageHeight());
    }

    public Sprite(Texture tex, Vector2f size) {
        texture = tex;
        this.size = size;
    }

    public void drawSprite(Vector2f pos, Color color) {
        color.bind();
        texture.bind();
        glBegin(GL_QUADS);
        glTexCoord2f(0, 0);
        glVertex2f(pos.x, pos.y); //Top left
        glTexCoord2f(1, 0);
        glVertex2f(pos.x + size.x, pos.y); //Top right
        glTexCoord2f(1, 1);
        glVertex2f(pos.x + size.x, pos.y + size.y); //Bottom right
        glTexCoord2f(0, 1);
        glVertex2f(pos.x, pos.y + size.y); //Bottom left
        glEnd();
    }

    public void drawSprite(Vector2f pos, Color color, int rot) {
        color.bind();
        texture.bind();
        glBegin(GL_QUADS);
        if (rot == 0 || rot == 360) {
            glTexCoord2f(0, 0);
            glVertex2f(pos.x, pos.y); //Top left
            glTexCoord2f(1, 0);
            glVertex2f(pos.x + size.x, pos.y); //Top right
            glTexCoord2f(1, 1);
            glVertex2f(pos.x + size.x, pos.y + size.y); //Bottom right
            glTexCoord2f(0, 1);
            glVertex2f(pos.x, pos.y + size.y); //Bottom left
        } else if (rot == 90) {
            glTexCoord2f(0, 1);
            glVertex2f(pos.x, pos.y); //Top left
            glTexCoord2f(0, 0);
            glVertex2f(pos.x + size.x, pos.y); //Top right
            glTexCoord2f(1, 0);
            glVertex2f(pos.x + size.x, pos.y + size.y); //Bottom right
            glTexCoord2f(1, 1);
            glVertex2f(pos.x, pos.y + size.y); //Bottom left
        } else if (rot == 180) {
            glTexCoord2f(1, 1);
            glVertex2f(pos.x, pos.y); //Top left
            glTexCoord2f(0, 1);
            glVertex2f(pos.x + size.x, pos.y); //Top right
            glTexCoord2f(0, 0);
            glVertex2f(pos.x + size.x, pos.y + size.y); //Bottom right
            glTexCoord2f(1, 0);
            glVertex2f(pos.x, pos.y + size.y); //Bottom left
        } else {
            glTexCoord2f(1, 0);
            glVertex2f(pos.x, pos.y); //Top left
            glTexCoord2f(1, 1);
            glVertex2f(pos.x + size.x, pos.y); //Top right
            glTexCoord2f(0, 1);
            glVertex2f(pos.x + size.x, pos.y + size.y); //Bottom right
            glTexCoord2f(0, 0);
            glVertex2f(pos.x, pos.y + size.y); //Bottom left
        }

        glEnd();
    }
}
