package com;

import com.objects.Player;
import com.state.*;
import com.state.StateManager;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.util.vector.Vector2f;

import java.io.File;
import java.lang.reflect.Field;
import java.util.Random;

import static org.lwjgl.Sys.getTime;
import static org.lwjgl.opengl.GL11.*;

public class Main {

    public static AssetManager assetManager;
    public static Random rand = new Random();
    public static Player player = new Player();
    public static StateManager stateManager;


    public static Vector2f cameraPos = new Vector2f(0, 0);


    public static float delta = 0;
    public static long lastTime = 0;


    public static boolean inMenu = true;

    private static void update() {
        long currentTime = getTime();
        Main.delta = (currentTime - lastTime) / 1000.0f;
        lastTime = currentTime;
        stateManager.checkInput();
        stateManager.update();

        if (inMenu && !assetManager.sounds.get("menu").isPlaying()) {
            assetManager.sounds.get("menu").playAsMusic(1, 1, true);
        } else if (!inMenu && assetManager.sounds.get("menu").isPlaying()) {
            assetManager.sounds.get("menu").stop();
        }
    }

    private static void render() {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glClearColor(1, 0.95f, 0.6f, 0);
        stateManager.render();
        Display.update();
    }

    private static void Init() {
        try {
            setDisplayMode(800, 600, false);
            Display.create();
            glViewport(0, 0, Display.getWidth(), Display.getHeight());
        } catch (Exception e) {
            e.printStackTrace();
        }

        glDisable(GL_DEPTH_TEST);
        glEnable(GL11.GL_BLEND);
        glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);

        glMatrixMode(GL11.GL_PROJECTION);
        glLoadIdentity();
        glOrtho(0, Display.getWidth(), Display.getHeight(), 0, -1, 1);
        glMatrixMode(GL_MODELVIEW);


        assetManager = new AssetManager();

        stateManager = new StateManager();
        stateManager.addState("stateEmpty", new StateEmpty());
        stateManager.addState("statePlaying", new StatePlaying());
        stateManager.addState("stateMenuMain", new StateMenuMain());
        stateManager.addState("stateMenuDifficulty", new StateMenuDifficulty());
        stateManager.addState("stateMenuPause", new StateMenuPause());
        stateManager.addState("stateMenuRespawn", new StateMenuRespawn());
        stateManager.addState("stateMenuComplete", new StateMenuComplete());

        stateManager.setState("stateMenuMain");


        player.tilePosition = new Vector2f(assetManager.checkpoints.get(0).x, assetManager.checkpoints.get(0).y);
        player.moveToTile = player.tilePosition;
        player.position.x = player.moveToTile.x * 64 + 16;
        player.position.y = player.moveToTile.y * 64 - 32;
        player.facing = (int) assetManager.checkpoints.get(0).z;


        while (!Display.isCloseRequested()) {
            update();
            render();
            Display.sync(60);
        }
        Display.destroy();
    }

    public static int roundTo(float i, int a) {
        return (int) Math.round(i / a) * a;
    }

    public static void setDisplayMode(int width, int height, boolean fullscreen) {

        // return if requested DisplayMode is already set
        if ((Display.getDisplayMode().getWidth() == width) &&
                (Display.getDisplayMode().getHeight() == height) &&
                (Display.isFullscreen() == fullscreen)) {
            return;
        }

        try {
            DisplayMode targetDisplayMode = null;

            if (fullscreen) {
                DisplayMode[] modes = Display.getAvailableDisplayModes();
                int freq = 0;

                for (int i = 0; i < modes.length; i++) {
                    DisplayMode current = modes[i];

                    if ((current.getWidth() == width) && (current.getHeight() == height)) {
                        if ((targetDisplayMode == null) || (current.getFrequency() >= freq)) {
                            if ((targetDisplayMode == null) || (current.getBitsPerPixel() > targetDisplayMode.getBitsPerPixel())) {
                                targetDisplayMode = current;
                                freq = targetDisplayMode.getFrequency();
                            }
                        }

                        if ((current.getBitsPerPixel() == Display.getDesktopDisplayMode().getBitsPerPixel()) &&
                                (current.getFrequency() == Display.getDesktopDisplayMode().getFrequency())) {
                            targetDisplayMode = current;
                            break;
                        }
                    }
                }
            } else {
                targetDisplayMode = new DisplayMode(width, height);
            }

            if (targetDisplayMode == null) {
                System.out.println("Failed to find value mode: " + width + "x" + height + " fs=" + fullscreen);
                return;
            }

            Display.setDisplayMode(targetDisplayMode);
            Display.setFullscreen(fullscreen);
            //glViewport(0, 0, Display.getWidth(), Display.getHeight());

        } catch (LWJGLException e) {
            System.out.println("Unable to setup mode " + width + "x" + height + " fullscreen=" + fullscreen + e);
        }
    }

    public static void main(String args[]) {
        try {
            System.setProperty("java.awt.headless", "true");

            String nativesPath = "natives" + File.separator;
            String os = System.getProperty("os.name").toUpperCase();
            if (os.contains("WIN")) nativesPath += "windows";
            else if (os.contains("MAC")) nativesPath += "macosx";
            else if (os.contains("NIX") || os.contains("NUX") || os.contains("aix")) nativesPath += "linux";
            else if (os.contains("SUNOS")) nativesPath += "solaris";

            System.setProperty("java.library.path", nativesPath);
            final Field sysPathsField = ClassLoader.class.getDeclaredField("sys_paths");
            sysPathsField.setAccessible(true);
            sysPathsField.set(null, null);
        } catch (Exception e) {
        }
        Init();
    }

}
