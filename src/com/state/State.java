package com.state;

public interface State {
    public void checkInput();

    public void update();

    public void render();

    public void onEnter();

    public void onExit();
}
