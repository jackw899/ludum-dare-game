package com.state;

import com.Main;
import org.lwjgl.input.Keyboard;

import static org.lwjgl.opengl.GL11.*;

public class StateMenuPause implements State {
    @Override
    public void checkInput() {
        if (Keyboard.isKeyDown(Keyboard.KEY_Q)) Main.stateManager.setState("stateMenuMain");
        while (Keyboard.next()) {
            if (Keyboard.getEventKeyState()) {
                if (Keyboard.getEventKey() == Keyboard.KEY_ESCAPE) Main.stateManager.setState("statePlaying");
            }
        }
    }

    @Override
    public void update() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void render() {
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
        glTranslatef(0, 0, 0);
        Main.assetManager.fonts.get("Tahoma20BoldBlack").drawString(5, 5, "Paused");
        Main.assetManager.fonts.get("Tahoma16PlainBlack").drawString(5, 40, "Press \"escape\" to resume");
        Main.assetManager.fonts.get("Tahoma16PlainBlack").drawString(5, 60, "Press \"q\" to go to main menu");
    }

    @Override
    public void onEnter() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void onExit() {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
